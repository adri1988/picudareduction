#include <cuda.h>
#include <stdio.h> 
#include <stdlib.h> 
#include <math.h> 
#include <cuda_runtime.h> 
/* Time */
#include <sys/time.h>
#include <sys/resource.h>

#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort=true)
{
   if (code != cudaSuccess) 
   {
      fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
      if (abort) exit(code);
   }
}




static struct timeval tv0;
double getMicroSeconds()
{
	double t;
	gettimeofday(&tv0, (struct timezone*)0);
	t = ((tv0.tv_usec) + (tv0.tv_sec)*1000000);

	return (t);
}

__global__ void reduce0(float *g_idata, float *g_odata) {
	extern __shared__ float sdata[];
	// each thread loads one element from global to shared mem
	unsigned int tid = threadIdx.x;
	unsigned int i = blockIdx.x*blockDim.x + threadIdx.x;
	sdata[tid] = g_idata[i]; 
    __syncthreads();
      
	// do reduction in shared mem

	for
		(unsigned int s = 1; s < blockDim.x; s *= 2) {		
		if (tid % (2 * s) == 0) {		
			sdata[tid] += sdata[tid + s];
				
		}
		__syncthreads();		
	}
	
	// write result for this block to global mem
	if (tid == 0) g_odata[blockIdx.x] = sdata[0];
}



__global__ void reduce1(float *g_idata, float *g_odata) {
	extern __shared__ float sdata[];
	// each thread loads one element from global to shared mem
	unsigned int tid = threadIdx.x;
	unsigned int i = blockIdx.x*blockDim.x + threadIdx.x;
	sdata[tid] = g_idata[i];  
    __syncthreads();
       

	for (unsigned int s=1; s < blockDim.x; s *= 2) {
		int index = 2 * s * tid;
		if (index < blockDim.x) {
			sdata[index] += sdata[index + s];
		}
		__syncthreads();
	}
	
	// write result for this block to global mem
	if (tid == 0) g_odata[blockIdx.x] = sdata[0];
}

__global__ void reduce2(float *g_idata, float *g_odata) {
	extern __shared__ float sdata[];
	// each thread loads one element from global to shared mem
	unsigned int tid = threadIdx.x;
	unsigned int i = blockIdx.x*blockDim.x + threadIdx.x;
	sdata[tid] = g_idata[i];  
    __syncthreads();
       

	for (unsigned int s=blockDim.x/2; s>0; s>>=1) {
		if (tid < s) {
			sdata[tid] += sdata[tid + s];
		}
	__syncthreads();
	}
	
	// write result for this block to global mem
	if (tid == 0) g_odata[blockIdx.x] = sdata[0];
}


__global__ void calcRectangles(float * rectangles,int n){
	int i = blockIdx.x * blockDim.x + threadIdx.x;
	float x;

	if (i >= 0 && i < n){
		x = (i + 0.5) / n;
		rectangles[i] = 4.0 / (1.0 + x*x);
	}
}


void copia (float * inputParcial,float * rectangles,int i){

	for (int j=0;j<32;j++)
			inputParcial[j]=rectangles[i*32 + j];

}

void printArray(float * array,int size, char * nombre){

	for (int i=0;i<size;i++){

		printf(" %s[%i]=%f \n",nombre,i,array[i]);
	}

}


float calcReductionGeneral(int n, float * rectangles,int total , int type){

	clock_t start; 		
	clock_t end; 
	float secondsTotal = 0;



	if (n<32){ //If the lenght of the array is less than 32, we make the reduction in the host

		start = clock();

		float * resultadoGlobaL = (float*)malloc(sizeof(float)*n);
		float pi;
		for (int i=0;i<n;i++)
				pi = pi + rectangles[i];
		pi = pi / total;
		end = clock();
		secondsTotal = ((float)(end - start) / CLOCKS_PER_SEC);
		printf ("pi = %f " ,pi);
		free(resultadoGlobaL);

		return secondsTotal;



	}
	else{ //reduction on GPU

		

		float * resParcial = (float*)malloc(sizeof(float)*n/32); //partial solution, array of 32 floats
		


		for (int i=0;i<n/32;i++){  

		 /*i.e.   if we have 64 rectangles we are going to make 2 kernel calls
	  			  first call with 0..31 elemets
	 			  second call with 32..63 elements*/ 


			float * inputParcial = (float*)malloc(sizeof(float)*32); //partial input to the kernel allocation
			float * input_d;
			gpuErrchk(cudaMalloc((void**)&input_d, 32*sizeof(float))); //GPU allocation
			copia(inputParcial,rectangles,i);//copy the relative 32 elements of the rectangles to the partial input

			/*  i.e  if we have 64 elements in the first loop iteration we copy the 0..31 elements and in the second iteration from 32..63 elements	
			*/			
			cudaMemcpy(input_d, inputParcial, sizeof(float)*32, cudaMemcpyHostToDevice);
			//We moved the parcial input to the GPU




			float * output;
			gpuErrchk(cudaMalloc((void**)&output, 32*sizeof(float)));	//allocating memory on GPU
			float * resParcial132 = (float*)malloc(sizeof(float)*32);   //allocating memory for parcial result
			
			start = clock();//we measure time
			switch (type){  //calling the kernel, using the type parameter
				case 0: reduce0 <<<1,32,32 >>>(input_d, output);	break;
				case 1: reduce1 <<<1,32,32 >>>(input_d, output);	break;
				case 2: reduce2 <<<1,32,32 >>>(input_d, output);	break;
				//case 3: reduce3 <<<1,32,32 >>>(input_d, output);	break;				
				default: break;
			}
			end = clock();
			secondsTotal = secondsTotal + ((float)(end - start) / CLOCKS_PER_SEC);

			
			gpuErrchk (cudaThreadSynchronize());
			gpuErrchk (cudaMemcpy(resParcial132,  output, sizeof(float)*32, cudaMemcpyDeviceToHost)); // we movce the result to the host
			//printf ("resParcial132[0] = %f" ,resParcial132[0]);
			gpuErrchk (cudaThreadSynchronize());
			resParcial[i]= resParcial132[0];


			//clean up
			free(inputParcial);
			free(resParcial132);
			cudaFree(input_d);	
			cudaFree(output);	

		}
		
		return calcReductionGeneral((int)n/32,resParcial,total,type) + secondsTotal;  //recursive call.
	}



}



int main(int argc, char **argv)
{
	int n;	

	if (argc == 2)
		n = atoi(argv[1]);
	else {
		n = 4194304;
		printf("./exec n (by default n=%i)\n", n);
	}

	

	/* Initizalization */

	//first we declarate the array with all the rectangles and allocate memory on the device
	float * rectangles_d;
	float * rectangles_dout;	
	//float * rectangles_dSection;
	float * rectangles_h;
	rectangles_h = (float*)malloc(sizeof(float)*n);

	

	gpuErrchk(cudaMalloc((void**)&rectangles_d, n*sizeof(float)));	
	gpuErrchk(cudaMalloc((void**)&rectangles_dout, 32*sizeof(float)));	
	dim3 GridBlocks(4096);
	dim3 threadsPerBlock(1024);//only 1 block with all the threads necesary to make the calculation
	calcRectangles <<<GridBlocks, threadsPerBlock >>>(rectangles_d,n);	

	gpuErrchk (cudaPeekAtLastError());
	gpuErrchk( cudaThreadSynchronize());
	gpuErrchk (cudaMemcpy(rectangles_h, rectangles_d, sizeof(float)*n, cudaMemcpyDeviceToHost));






	
	float segundos = calcReductionGeneral(n,rectangles_h,n,0);   
    printf("Time reduce0: %f seconds\n",segundos);

    segundos = calcReductionGeneral(n,rectangles_h,n,1);   
    printf("Time reduce1: %f seconds\n",segundos);

    segundos = calcReductionGeneral(n,rectangles_h,n,2);   
    printf("Time reduce2: %f seconds\n",segundos);  


	cudaFree(rectangles_d);
    cudaFree(rectangles_dout); 
    free(rectangles_h);
   
    


    return 0;
		
	

      
}